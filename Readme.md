R�ponses aux questions :

/********** S�rie 1 **********/

--> Exo 2
1/ Les m�thodes cr�es sont :
- Factorielle : Calcule la factorielle d'un nombre donn� de type int, double ou BigInteger.
- Main : Affiche les factorielles de 10, 13 et 25 avec diff�rents types en utilisant la classe Factorielle.

2/ Voici les r�sultats calcul�s par le programme :
- Factorielle de int 10 = 3628800
- Factorielle de double 10 = 3628800.0
- Factorielle de BigInteger 10 = 3628800
Pour l'instant, on ne remarque pas de grosses diff�rences entre les diff�rents types.

- Factorielle de int 13 = 1932053504
- Factorielle de double 13 = 6.2270208E9
- Factorielle de BigInteger 13 = 6227020800
Maintenant on commence � remarquer un �cart entre ces diff�rentes valeurs.
Le type int n'est plus appropri� pour de telles valeurs.

- Factorielle de int 25 = 2076180480
- Factorielle de double 25 = 1.5511210043330986E25
- Factorielle de BigInteger 25 = 15511210043330985984000000
On remarque maintenant que le type BigInteger permet d'avoir une valeur plus pr�cise !


--> Exo 3
1/ On cr�e une classe Bissextille qui prend en argument l'ann�e et retourne "true" si l'ann�e est bissextile
et "false" si l'ann�e n'est pas bissextile.

2/ Pour les ann�es donn�es en exemple : 
- 1900 n'est pas une ann�e bissextile
- 1901 n'est pas une ann�e bissextile
- 1996 est bien une ann�e bissextile
- 2000 est bien une ann�e bissextile


--> Exo 4
1/ 2/ On cr�e une classe Palindrome qui prend en argument un String et retourne "true" si la phrase est un
palindrome et "false" sinon.

3/ En testant les phrases donn�es dans la classe Main, elles sont bien toutes des palindromes.

La m�thode charAt de la classe String renvoie le caract�re correspondant � la position indiqu�e.
Exemple : Soit String mot = "Bonjour" alors mot.charAt(3) renvoie j.
L'�quivalent en C (grossi�rement) est mot[3] qui renvoie j.


/********** S�rie 2 **********/ 

--> Exo 5
1/ Premier constructeur : public Marin(String nouveauNom, String nouveauPrenom, int nouveauSalaire)

2/ Deuxi�me constructeur : public Marin(String nouveauNom, int nouveauSalaire)

3/ Pour cela deux m�thodes :
Soit on le fait � la main comme nous l'avons fait pour getNom() et setNom().

4/ Soit on g�n�re ces fonctions int�gr�s � Eclipse (beaucoup plus rapide et aussi efficace).
On fait pour cela Alt + Shift + S ==> Generate Getters and Setters
On l'a fait pour getPrenom, getSalaire, setNom et setSalaire.

5/ public void augmenteSalaire(int augmentation)

6/ Pour cr�er des objets d'une classe Marin :
Marin m1 = new Marin("Mohammad", "Akash",10500);

7/ Pour afficher directement les diff�rents champs avec System.out.println qui prend en argumant un String
nous allons g�n�rer dans la classe Marin (Alt+Shift+S) la fonction toString().

8/ Pour comparer deux instances, on va utiliser la m�thode equals().
On la g�n�re de la m�me fa�on. En la g�n�rant, on g�n�re �galement la m�thode hashCode().

9/ On peut faire : m1 = m2;

10/ Les codes de hachages obtenus sont : 
m1 : 933554165
m2 : 933554165
m3 : -58901893

On doit bien obtenir un code hachage identique pour m1 et m2, ces deux �tant identiques.
m3 �tant diff�rent de m1 et m2, le code de hachage doit �tre diff�rent ce qui est le cas.

11/ A) On va impl�menter � notre classe Marin l'interface Comparable<Marin> et cr�er la m�thode compareTo
qui va comparer les noms des marins puis les pr�noms si besoin.

B) En comparant plusieurs marins, on peut affirmer que le programme fonctionne correctement.


--> Exo 6
Dans cet exercice, on a import� la classe Marin, en faisant un copier-coller.
On cr�e un tableau de Marin qui contient 5 marins. 
Pour cr�er les diff�rentes m�thodes demand�es dans l'�nonc�, on va utiliser les Getters et les Setters cr��s pr�c�demment (getSalaire et setSalaire).

On teste ensuite ces m�thodes sur le tableau de Marin.
Les m�thodes fonctionnent correctement.

/********** S�rie 6 **********/

--> Exo 14
1/ Le fichier contient 20064 lignes.
Le roman uniquement contient 19672 lignes.

2/ Le texte comporte 16299 lignes non vides.

3/ Il appara�t 7 fois.

6/ Les caract�res de Germinal sont :
[ , !, ', *, ,, -, ., 0, 1, 2, 4, 5, 6, 7, 8, 9, :, ;, <, >, ?, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, X, Y, Z, _, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z]

7/ Germinal comporte :
- 177195 mots au total
- 13941 mots mots distincts

8/ La longueur du mot le plus long est : 19
Il y a deux mots de cette longueur : proportionnellement et revolutionnairement

9/ Longueur de mots la plus fr�quente : 2
Il y a 42717 mots de cette longueur 

10/ Longueur m�diane est 4.
Il y a 21849 mots de cette longueur.

/********** S�rie 7 **********/

--> Exo 15
2/A/ On doit utiliser le flux DataOutputStream.

3/A/ On doit utiliser le flux ObjectOutputStream.