package org.mohammad.serie08.exo16;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class AnalyzeBean implements Serializable
{
	private static final long serialVersionUID = 1L;

	public AnalyzeBean() 
	{		
	}
	
	public String getClassName(Object o)
	{
		String className;
		
		className = o.getClass().getSimpleName();
		System.out.println(" 1/ Le nom de la classe de p1 est : " + className);
		
		className = o.getClass().getName();	// Retourne le package avac la classe
		
		return className;
	}
	
	public Object getInstance(String className)
	{
		Class<?> cls;
		Object o = null;
		
		try 
		{
			cls = Class.forName(className);
			try 
			{
				o = cls.newInstance();
			} 
			catch (InstantiationException e) 
			{
				System.out.println("Erreur : " + e.getMessage());
				e.printStackTrace();
			} 
			catch (IllegalAccessException e) 
			{
				System.out.println("Erreur : " + e.getMessage());
				e.printStackTrace();
			}
			
			return o;
		} 
		catch (ClassNotFoundException e) 
		{
			System.out.println("Erreur :" + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<String> getProperties(Object o)
	{
		Class<?> cls = o.getClass();
		Function<String,String> extractPropertyName = name -> name.substring(3,4).toLowerCase() + name.substring(4,name.length());
		
		@SuppressWarnings("unused")
		Predicate<String> isGetterPresent = pn -> 
		{
			String g = "get" + pn.substring(0,1).toUpperCase() + pn.substring(1, pn.length());
			try 
			{
				Method getter = cls.getMethod(g);
				return true;
			} 
			catch (NoSuchMethodException e) 
			{
				return false;
			} 
			catch (SecurityException e) 
			{
				return false;
			}
		};
		
		List<String> property = Arrays
				.stream(cls.getMethods())
				.map(m -> m.getName())
				.filter(name -> (name.startsWith("set")))
				.map(extractPropertyName)
				.filter(isGetterPresent)
				.collect(Collectors.toList());
		
		return property;
	}
	
	public Object get(Object bean, String property)
	{
		Object value = null;
		List<String> prop = getProperties(bean);
		
		for(int i = 0; i < prop.size() ; i++)
		{
			if(property.equals(prop.get(i)))
			{
				String s = "get" + prop.get(i).substring(0, 1).toUpperCase() + prop.get(i).substring(1, prop.get(i).length());
				try 
				{
					Method m = bean.getClass().getMethod(s);
					
					try 
					{
						value = m.invoke(bean);
					} 
					catch (IllegalAccessException e) 
					{
						System.out.println("Erreur : " + e.getMessage());
						e.printStackTrace();
					} 
					catch (IllegalArgumentException e) 
					{
						System.out.println("Erreur : " + e.getMessage());
						e.printStackTrace();
					} 
					catch (InvocationTargetException e) 
					{
						System.out.println("Erreur : " + e.getMessage());
						e.printStackTrace();
					}
				} 
				catch (NoSuchMethodException e) 
				{
					System.out.println("Erreur : " + e.getMessage());
					e.printStackTrace();
				} 
				catch (SecurityException e) 
				{
					System.out.println("Erreur : " + e.getMessage());
					e.printStackTrace();
				}
			}
		}

		return value;
	}
	
	public void set(Object bean, String property, Object value)
	{
		List<String> prop = getProperties(bean);
		
		for(int i = 0; i < prop.size() ; i++)
		{
			if(property.equals(prop.get(i)))
			{
				String getter = "get" + prop.get(i).substring(0, 1).toUpperCase() + prop.get(i).substring(1, prop.get(i).length());
				String setter = "set" + prop.get(i).substring(0, 1).toUpperCase() + prop.get(i).substring(1, prop.get(i).length());
				
				try 
				{
					Method methods = bean.getClass().getMethod(getter);
					Class<?> parametreType = methods.getReturnType();
					methods = bean.getClass().getMethod(setter, parametreType);
					
					try 
					{
						methods.invoke(bean, value);
					} 
					catch (IllegalAccessException e) 
					{
						System.out.println("Erreur : " + e.getMessage());
						e.printStackTrace();
					} 
					catch (IllegalArgumentException e) 
					{
						System.out.println("Erreur : " + e.getMessage());
						e.printStackTrace();
					} 
					catch (InvocationTargetException e) 
					{
						System.out.println("Erreur : " + e.getMessage());
						e.printStackTrace();
					}
				} 
				catch (NoSuchMethodException e) 
				{
					System.out.println("Erreur : " + e.getMessage());
					e.printStackTrace();
				} 
				catch (SecurityException e) 
				{
					System.out.println("Erreur : " + e.getMessage());
					e.printStackTrace();
				}
			}
		}
	}
	
	public List<Object> read(String filename)
	{
		List<Object> listObject = new ArrayList<>();
		try
		{
			InputStream ips = new FileInputStream(filename);
			InputStreamReader ipsr = new InputStreamReader(ips);
			BufferedReader br = new BufferedReader(ipsr);
			String ligne;
			Object obj = null;
			AnalyzeBean ab = new AnalyzeBean();
			List<String> propriete = new ArrayList<>();
			
			while ((ligne = br.readLine()) != null)
			{
				if(!ligne.startsWith("#"))
				{
					if(ligne.contains("bean.name="))
					{
						obj = ligne.substring(10);
					}
					if(ligne.contains(obj+".class"))
					{
						String className = ligne.substring((obj+".class=").length());
						Object instance = ab.getInstance(className);
						propriete = ab.getProperties(instance);
						listObject.clear();
						System.out.println("L'objet est : " + obj);
						System.out.println("Sa classe est : " + className);
						System.out.println("L'instance est donc : " + instance);
						System.out.println("Les propriétes de cette classes sont : " + propriete);
					}
					for(int i = 0; i < propriete.size(); i++)
					{
						if(ligne.contains(obj+"."+propriete.get(i)))
							listObject.add(propriete.get(i) + " = " + ligne.substring((obj+"."+propriete.get(i)+"=").length()));
					}				
					if((listObject.size() == propriete.size()) && (ligne.contains(obj+".")))
						System.out.println("-->" + listObject + "\n");
				}
			}
			br.close(); 
		}
		catch(IOException e)
		{
			System.out.println("Erreur : " + e.getMessage());
		}
		
		return listObject;
	}
}