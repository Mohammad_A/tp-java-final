package org.mohammad.serie08.exo16;

import java.util.*;

public class Main 
{
	public static void main(String[] args)
	{
		// Q1 
		String className;
		Person p1 = new Person();
		Employee employee = new Employee(); 
		AnalyzeBean ab = new AnalyzeBean();
		
		className = ab.getClassName(p1);
		System.out.println(" --> Le nom complet (avec packages) de la classe de p1 est : " + className + "\n");
		
		// Q2
		Person p2 = (Person) ab.getInstance(className);
		System.out.println("2/ Instance de la classe Person :");
		System.out.println(p2);
		
		System.out.println("");
		
		// Q3
		List<String> listProperty = new ArrayList<>();
		System.out.println("3/ Affichage des propri�t�s de la classe Person puis de la classe Employee :");
		
		listProperty = ab.getProperties(p1);
		System.out.println("--> Person : " + listProperty);
		
		listProperty = ab.getProperties(employee);
		System.out.println("--> Employee : " + listProperty);
		
		System.out.println("");
		
		// Q4
		Person p3 = new Person("Mohammad", "Tanuja", 21); 
		Object pr = ab.get(p3, "firstName");
		
		System.out.println("4/ La valeur de la propri�t� demand�e (ici firstName) de p3 est : " + pr);
		System.out.println("--> p3 : " + p3);
		System.out.println("");
		
		// Q5
		System.out.println("5/ Donner des valeurs aux propri�t�s de p2 et d'employee : ");
		
		System.out.println("--> p2 avant la m�thode set : " + p2);
		ab.set(p2, "firstName", "Akash");
		ab.set(p2, "lastName", "Mohammad");
		ab.set(p2, "age", 21);
		System.out.println("--> p2 apr�s la m�thode set : " + p2);
		
		System.out.println("--> employee avant la m�thode set : " + employee);
		ab.set(employee, "firstName", "Lionel");
		ab.set(employee, "lastName", "Messi");
		ab.set(employee, "age", 29);
		ab.set(employee, "salary", 2000000);
		System.out.println("--> employee apr�s la m�thode set : " + employee);
		
		System.out.println("");
		
		//  Q6
		System.out.println("6/ Lecture du fichier texte : ");
		ab.read("src/org/mohammad/serie08/exo16/Texte.txt");
		
		System.out.println("");
		
	}
}