package org.mohammad.serie08.exo16;

import java.io.Serializable;

public class Employee extends Person implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private int salary;
	
	public Employee()
	{	
	}

	public int getSalary() 
	{
		return salary;
	}

	public void setSalary(int salary) 
	{
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Employee [ firstName = " + getFirstName() + ", lastName = " + getLastName()
				+ ", age = " + getAge() + ", salary = " + salary + "]";
	}
}