package org.mohammad.serie08.exo16;

import java.io.Serializable;

public class Person implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String firstName;
	private String lastName;
	private int age;
	
	public Person()
	{	
	}
	
	public Person(String nom, String prenom, int age)
	{
		this.lastName = nom;
		this.firstName = prenom;
		this.age = age;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	public void setAkash()
	{
	}

	@Override
	public String toString() {
		return "Person [firstName = " + firstName + ", lastName = " + lastName + ", age = " + age + "]";
	}
}