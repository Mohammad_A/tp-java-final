package org.mohammad.serie05.exo12;

import java.util.*;
import java.util.function.*;


public class Main 
{
	public static void main(String[] args)
	{
		List<String> strings = new ArrayList<>();
		
		// Ajout des �l�ments � la liste
		strings.add("one");
		strings.add("two");
		strings.add("three");
		strings.add("four");
		strings.add("five");
		strings.add("six");
		strings.add("seven");
		strings.add("eight");
		strings.add("nine");
		strings.add("ten");
		strings.add("eleven");
		strings.add("twelve");
		
		//Affichage de la liste
		System.out.println("--> Affichage de la liste strings (avec println) : \n" + strings + "\n");
		
		// Q1
		System.out.println("1/ Affichage de la liste strings (avec forEach) : ");
		strings.forEach(s -> System.out.println(s));
		System.out.println("");

		// Q2
		List<String> strings3Char = new ArrayList<>();
		Predicate<String> notEqualToThree = s -> s.length() != 3;
		
		strings3Char.addAll(strings);
		strings3Char.removeIf(notEqualToThree);
		
		System.out.println("2/ Affichage de la liste comportant 3 caract�res : ");
		strings3Char.forEach(s -> System.out.println(s));
		System.out.println("");

		// Q3
		Comparator<String> compareLongueur = Comparator.comparing(s -> s.length());
		strings.sort(compareLongueur);
		
		System.out.println("3/ Affichage de la liste strings tri�e selon la longueur des cha�nes : ");
		System.out.println(strings + "\n");
		
		// Q4
		Map<Integer, List<String>> map1 = new HashMap<>();
		
		strings.forEach(s -> {							// Pour tous les �l�ments de strings
			List<String> liste;
			if (map1.get(s.length()) == null) 			// Si la cl� n'exicte pas 
				liste = new ArrayList<>();				// On cr�e une nouvelle liste
			else 										// Sinon
				liste = map1.get(s.length());			// On se prend la liste d�j� existante 
			liste.add(s);								// On ajoute la chaine � cette liste
			map1.put(s.length(), liste);				// On donne cette nouvelle liste � la table
			});
		
		System.out.println("4/ Affichage de la table de hachage (cl� --> Longueur des cha�nes / valeur --> Liste des cha�nes) : ");
		map1.forEach((key,value) -> System.out.println("key = " + key + " , value = " + value));
		System.out.println("");
		
		// Q5
		Map<Character, List<String>> map2 = new HashMap<>();
		
		strings.forEach(s -> {
			List<String> list;
			if(map2.get(s.charAt(0)) == null)
				list = new ArrayList<>();
			else
				list = map2.get(s.charAt(0));
			list.add(s);
			map2.put(s.charAt(0), list);
		});
		
		System.out.println("5/ Affichage de la table de hachage (cl� --> 1�re lettre des cha�nes / valeur --> Liste des cha�nes) : ");
		map2.forEach((key,value) -> System.out.println("key = " + key + " , value = " + value));
		System.out.println("");
		
		// Q6
		Map<Character, Map<Integer,List<String>>> map3 = new HashMap<>();
		
		strings.forEach(s -> {
			Map<Integer,List<String>> mapValues;
			List<String> listValues;
			if(map3.get(s.charAt(0)) == null)
			{
				mapValues = new HashMap<>();
				listValues = new ArrayList<>();
			}
			else
			{
				mapValues = map3.get(s.charAt(0));
				if(mapValues.get(s.length()) == null)
					listValues = new ArrayList<>();
				else
					listValues = mapValues.get(s.length());
			}
			listValues.add(s);
			mapValues.put(s.length(), listValues);
			map3.put(s.charAt(0), mapValues);
		});
		
		System.out.println("6/ Affichage de la table de hachage (cl� --> 1�re lettre des cha�nes / valeur --> Map) : ");
		map3.forEach((key,value) -> System.out.println("key = " + key + " , value = " + value));
		System.out.println("");
		
		// Q7
		Map<Integer, String> map4 = new HashMap<>();
		
		strings.forEach(s -> {
			String concatenation;
			if(map4.get(s.length()) == null)
				concatenation = new String();
			else
			{
				concatenation = map4.get(s.length());
				concatenation = concatenation + ",";
			}
			concatenation = concatenation + s;
			map4.put(s.length(), concatenation);
		});
		
		System.out.println("7/ Affichage de la table de hachage (cl� --> Longueur des cha�nes / valeur --> Cha�ne concat�n�e s�par�e par ',') : ");
		map4.forEach((key,value) -> System.out.println("key = " + key + " , value = " + value));
		System.out.println("");
	}
}