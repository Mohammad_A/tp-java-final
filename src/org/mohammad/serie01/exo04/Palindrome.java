package org.mohammad.serie01.exo04;

public class Palindrome 
{
	public boolean palindrome(String phrase)
	{
		int j = 0;												
		boolean resultat = false;									
		int longueur = phrase.length();							
		char [] envers = new char[longueur];					
		
		// Mettre la phrase en minuscule
	    phrase = phrase.toLowerCase();						
	    
	    // On �crit la phrase � l'envers dans le tableau envers[]
	    for (int i = 0; i < longueur; i++)
	    {
	        envers[i] = phrase.charAt(longueur - i - 1);		// phrase.charAt[i] ==> phrase[i] 
	    }

	    // On parcours les deux chaines et on compare chaque caract�re 
	    for(int i = 0; i < longueur; i++)
	    { 
	    	// Si le premier caract�re est identique on fixe bool = true
	    	// Et on continue � comparer le reste tant que les caract�res ne diff�rent pas
	    	if(phrase.charAt(i) == envers[j])
	    	{
	    		resultat = true;
	    	}
	    	// Sinon bool = false et on retourne la valeur sans continuer
	    	else
	    	{
	    		resultat = false;
	    		return resultat;
	    	}
	    	
	    	// Si une des deux chaines contient un espace, on passe au caract�re suivant
	    	if(i+1 < longueur)
	    	{
	    		if(phrase.charAt(i+1) == ' ' )					// Si caract�re == Espace
	    		{
	    			i++;
	    		}
	    	}
	    	
	    	if(j+1 < longueur)
	    	{
		    	if(envers[j+1] == ' ')							// Si caract�re == Espace
		    	{
		    		j++;
		    	}
	    	}
	    	
	    	j++;
	    }
	    
		return resultat;
	}
	
}