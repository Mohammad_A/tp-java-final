package org.mohammad.serie01.exo03;

public class Bissextile 
{
	public boolean bissextile(int annee)
	{
		boolean resultat = false;
		
		if(((annee % 4 == 0) && (annee % 100 != 0)) || (annee % 400 == 0))
		{
			resultat = true;
		}
		return resultat;
	}
}