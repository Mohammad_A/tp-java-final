package org.mohammad.serie01.exo02;

import java.math.BigInteger;

public class Main 
{
	public static void main(String[] args)
	{
		Factorielle fact = new Factorielle();
		
		//Factorielle de 10 avec les 3 types (int, double, BigInteger)
		System.out.println("--> Factorielles de 10 :");
		System.out.println("La factorielle de 10 (int) est : " + fact.factorielle1(10));
		System.out.println("La factorielle de 10 (double) est : " + fact.factorielle2(10));
		BigInteger nbr = new BigInteger("10");
		System.out.println("La factorielle de 10 (BigInteger) est : " + fact.factorielle3(nbr));
		System.out.println("");
		
		//Factorielle de 13 avec les 3 types (int, double, BigInteger)
		System.out.println("--> Factorielles de 13 :");
		System.out.println("La factorielle de 13 (int) est : " + fact.factorielle1(13));
		System.out.println("La factorielle de 13 (double) est : " + fact.factorielle2(13));
		nbr = new BigInteger("13");
		System.out.println("La factorielle de 13 (BigInteger) est : " + fact.factorielle3(nbr));
		System.out.println("");
		
		//Factorielle de 25 avec les 3 types (int, double, BigInteger)
		System.out.println("--> Factorielles de 25 :");
		System.out.println("La factorielle de 25 (int) est : " + fact.factorielle1(25));
		System.out.println("La factorielle de 25 (double) est : " + fact.factorielle2(25));
		nbr = new BigInteger("25");
		System.out.println("La factorielle de 25 (BigInteger) est : " + fact.factorielle3(nbr));
		System.out.println("");
	}
}