package org.mohammad.serie06.exo13;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main 
{
	public static void main(String[] args)
	{
		Stream<String> strings = Stream.of("one","two","three","four","five","six","seven","eight","nine","ten","eleven","twelve");
		List<String> list = strings.collect(Collectors.toList());
		
		// Q1
		System.out.println("1/ Affichage de la longueur de la cha�ne la plus longue : ");
		Comparator<String> compareLongueur = Comparator.comparing(s -> s.length());
		System.out.println(list.stream().max(compareLongueur).get().length());
		System.out.println("");
		
		// Q2
		System.out.println("2/ Affichage du nombre de cha�nes de longueur paire : ");
		long nbrLongueurPaire = 0;
		nbrLongueurPaire = list.stream().filter(s -> s.length() % 2 == 0).count();
		System.out.println(nbrLongueurPaire);
		System.out.println("");
		
		// Q3
		System.out.println("3/ Affichage de la liste avec longueur impaire : ");
		Stream<String> listeImpaire;
		listeImpaire = list.stream().filter(s -> s.length() % 2 != 0).map(s -> s.toUpperCase());
		listeImpaire.collect(Collectors.toList()).forEach(s -> System.out.println(s));
		System.out.println("");
		
		// Q4
		System.out.println("4/ Concat�nation des cha�nes de longueur <= 5 :");
		String concatenation;
		concatenation = list.stream().filter(s -> s.length() <= 5).sorted().collect(Collectors.joining(",","{","}"));
		System.out.println(concatenation);
		System.out.println("");
		
		// Q5
		System.out.println("5/ Affichage de la table de hachage avec comme cl� la longueur : ");
		Map<Integer,List<String>> map1 = new HashMap<>();
		map1 = list.stream().collect(Collectors.groupingBy(s -> s.length()));
		System.out.println(map1);
		System.out.println("");
		
		// Q6
		System.out.println("6/ Affichage de la table de hachage avec comme cl� la premi�re lettre : ");
		Map<String,String> map2 = new HashMap<>();
		map2 = list.stream().collect(Collectors.groupingBy(s -> s.substring(0,1), Collectors.joining(",", "{", "}")));
		System.out.println(map2);
		System.out.println("");
	}
}