package org.mohammad.serie06.exo14;

import java.util.*;
import java.util.Map.Entry;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main 
{
	public static void main(String[] args)
	{
		// Q1
		List<String> list = listOfLines();																				// Fichier Entier
		List<String> listLine = list.stream().limit(list.size() - 322).skip(70).collect(Collectors.toList());			// Uniquement le roman
		
		System.out.println("1/ Nombre de lignes :");
		System.out.println("Il y a " + list.size() + " lignes dans le fichier !");
		System.out.println("Il y a " + listLine.size() + " lignes dans le roman ! \n");
		
		// Q2
		long emptyLines = listLine.stream().filter(l -> l.isEmpty()).count();
		System.out.println("2/ Nombres de lignes vides : ");
		System.out.println("Il y a " +  emptyLines + " lignes vides dans le roman !");
		System.out.println("Il y a " +  (listLine.size() - emptyLines) + " lignes non vides dans le roman !\n");
		
		// Q3
		//long bonjour = listLine.stream().filter(l -> (l.contains("Bonjour")) || l.contains("bonjour")).count();
		long bonjour = listLine.stream().map(l -> l.toLowerCase()).filter(l -> l.contains("bonjour")).count();
		System.out.println("3/ Nombre de fois le mot <bonjour> appara�t : ");
		System.out.println("Il appara�t " + bonjour + " fois \n");
		
		// Q4
		//Function<String, Stream<Character>> charStream = s -> s.chars().mapToObj(i -> (char) i);
		Function<String, Stream<Character>> charStream = s -> s.chars().mapToObj(i -> (char) i);
		System.out.println("4/ Stream de caract�re � partir d'un String : <Bonjour Salut>");
		charStream.apply("Bonjour Salut").forEach(c -> System.out.println(c));
		System.out.println("");
		
		// Q5
		System.out.println("5/ Stream de caract�re du roman Germinal : ");
		listLine.forEach(l -> charStream.apply(l));
		System.out.println("");
		
		// Q6
		List<Character> charList = new ArrayList<>();
		//listLine.forEach(l -> charStream.apply(l).distinct().sorted().forEach(c -> charList.add(c)));
		listLine.stream().flatMap(l -> charStream.apply(l)).distinct().sorted().forEach(c -> charList.add(c));
		System.out.println("6/ Liste des caract�res pr�sents dans le roman Germinal :");
		System.out.println(charList + "\n");
		
		// Q7
		BiFunction<String, String, Stream<String>> splitWordWithPattern = (line, pattern) -> Pattern.compile("[" + pattern + "]").splitAsStream(line);
		//listLine.stream().forEach(l -> splitWordWithPattern.apply(l, " !'*,\\-.:;<>?_").forEach(w -> System.out.println(w)));
		System.out.println("7/ Nombre de mots au total et distinct dans le roman : ");
		long nbrMotsTotal = listLine.stream().flatMap(l -> splitWordWithPattern.apply(l, " !'*,\\-.:;<>?_")).filter(l -> l.length() != 0).count();
		System.out.println("--> Il y a " + nbrMotsTotal + " mots au total dans le roman");
		long nbrMotsDistinct = listLine.stream().flatMap(l -> splitWordWithPattern.apply(l, " !'*,\\-.:;<>?_")).filter(l -> l.length() != 0).distinct().count();
		System.out.println("--> Il y a " + nbrMotsDistinct + " mots distincts dans le roman");
		System.out.println("");
		
		// Q8
		System.out.print("8/ Longueur du mot le plus long utilis� dans le roman : ");
		int wordMaxLength = listLine.stream().flatMap(l -> splitWordWithPattern.apply(l, " !'*,\\-.:;<>?_")).map(l -> l.length()).max(Comparator.naturalOrder()).get();
		System.out.println(wordMaxLength);
		
		long nbrMaxWord = listLine.stream().flatMap(l -> splitWordWithPattern.apply(l, " !'*,\\-.:;<>?_")).filter(l -> l.length() == 19).distinct().count();
		System.out.println("--> Il y a " + nbrMaxWord + " mots de cette taille");
		
		System.out.println("--> Ces mots de 19 caract�res sont :");
		listLine.stream().flatMap(l -> splitWordWithPattern.apply(l, " !'*,\\-.:;<>?_")).filter(l -> l.length() == 19).distinct().forEach(w -> System.out.println("- " + w));
		System.out.println("");
		
		// Q9
		Map<Integer, Long> histogramme = new HashMap<>();
		histogramme = listLine.stream().flatMap(l -> splitWordWithPattern.apply(l, " !'*,\\-.:;<>?_")).filter(l -> l.length() >= 2).collect(Collectors.groupingBy(s -> s.length(), Collectors.counting()));
		System.out.println("9/ Affichage de l'histogramme des mots de Germinal --> key = longueur des mots , value = nombre de mots de cette longueur : ");
		System.out.println(histogramme);
		//histogramme.forEach((k,v) -> System.out.println("key = " + k + ", value = " + v)); 	AFFICHER LA TABLE AVEC FOREACH
		
		Entry<Integer, Long> frequentLength = histogramme.entrySet().stream().max(Map.Entry.comparingByValue()).get();
		System.out.println("--> La longueur de mots la plus fr�quente est : " + frequentLength.getKey());
		System.out.println("--> Le nombre de mots de cette longueur est : " + frequentLength.getValue() + " mots\n");
		
		// Q10
		Iterator<Entry<Integer, Long>> iter = histogramme.entrySet().stream().iterator();
		int medianValue = 0;
		int medianKey = 0;
		int nbrMots = 0;
		int stop = -1;
		
		while(iter.hasNext())
			nbrMots = nbrMots + iter.next().getValue().intValue();
		
		for(Entry<Integer, Long> entry : histogramme.entrySet())
		{
			if((medianValue < nbrMots/2) && (medianValue != -1))
				medianValue = medianValue + entry.getValue().intValue();
			if(medianValue >= nbrMots/2)
			{
				medianKey = entry.getKey().intValue();
				medianValue = stop;
			}
		}
		
		System.out.println("10/ La longueur m�diane de cet histogramme est : " + medianKey);
		System.out.println("--> Le nombre de mots de cette longueur est " + histogramme.get(medianKey) + " mots");
	}
	
	public static List<String> listOfLines ()
	{
		Path path = Paths.get("src/org/mohammad/serie06/exo14/7germ10.txt");
		try (Stream<String> lines = Files.lines(path))
		{
			return lines.collect(Collectors.toList());
		} 
		catch (IOException e)
		{
			System.out.println("e.getMessage() = " + e.getMessage());
		}
		
		return null;
	}
}