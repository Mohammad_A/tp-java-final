package org.mohammad.serie07.exo15;

import java.io.Serializable;

public class Person implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String lastName;
	private String firstName;
	private int age;
	private int salaire;
	
	public Person()
	{
		
	}
	
	public Person(String lName, String fName, int newAge)
	{
		this.lastName = lName;
		this.firstName = fName;
		this.age = newAge;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	public int getSalaire() {
		return salaire;
	}

	public void setSalaire(int salaire) {
		this.salaire = salaire;
	}

	public String toString() {
		return "Person [lastName = " + lastName + ", firstName = " + firstName + ", age = " + age + "]";
	}
}