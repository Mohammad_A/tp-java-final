package org.mohammad.serie07.exo15;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Writer;
import java.util.*;

public class PersonWriter 
{
	public void write(List<Person> people, String filename)
	{
		File fichier = new File(filename);
		Writer writer = null;
		try
		{
			writer = new FileWriter(fichier,true);
			for(Person p : people)
			{
				String chaine = p.getLastName() + "," + p.getFirstName() + "," + p.getAge() + "\r\n" ;
				writer.write(chaine);
			}
		}
		catch(IOException e)
		{
			System.out.println("Erreur" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			if(writer != null)
			{
				try
				{
					writer.close();
				}
				catch(IOException e)
				{
					System.out.println("Erreur" + e.getMessage());
				}
			}
		}
	}
	
	public void writeBinaryFields(List<Person> people, String fileName)
	{
		File fichier =  new File("src/org/mohammad/serie07/exo15/" + fileName);
		   
		try (FileOutputStream fos =  new FileOutputStream(fichier);
			DataOutputStream dos = new DataOutputStream(fos);)				// Flux pour �crire les tpyes primitifs JAVA
		{
			people.forEach(p -> {
				try 
				{
					dos.writeUTF(p.getLastName());							// writeUTF ==> String
					dos.writeUTF(p.getFirstName());
					dos.writeInt(p.getAge());								// writeInt ==> Integer
				} 
				catch (IOException e) 
				{
					System.out.println("Erreur : " + e.getMessage());
					e.printStackTrace();
				}
			});
		} 
		catch (IOException e) 
		{
			System.out.println("Erreur : " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void writeBinaryObject(List<Person> people, String fileName)
	{
		File fichier =  new File("src/org/mohammad/serie07/exo15/" + fileName);
		
		try (FileOutputStream fos = new FileOutputStream(fichier);
				ObjectOutputStream oos = new ObjectOutputStream(fos);)
		{
			oos.writeObject(people);
		} 
		catch (FileNotFoundException e) 
		{
			System.out.println("Erreur : Fichier non trouv� ! " + e.getMessage() );
		} 
		catch (IOException e) 
		{
			System.out.println("Erreur : " + e.getMessage());
		}
	}
}