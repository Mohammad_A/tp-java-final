package org.mohammad.serie07.exo15;

import java.util.*;

public class Main 
{
	public static void main(String[] args)
	{
		// Q1B
		PersonReader pr = new PersonReader();
		System.out.println("1/B/ Lecture du fichier Person.txt (Contenant 3 personnes) : ");
		pr.read("src/org/mohammad/serie07/exo15/Person.txt");
		System.out.println("");
		
		// Q1C
		Person p1 = new Person("Mohammad","Akash",21);
		Person p2 = new Person("Khan","Shahrukh",51);
		PersonWriter pw = new PersonWriter();
		List<Person> listPerson = new ArrayList<>();
		
		listPerson.add(p1);
		listPerson.add(p2);

		//pw.write(p1, "C:/Users/11314043/Desktop/Serie5/Serie7/PersonWrite.txt");
		//pw.write(p2,"C:/Users/11314043/Desktop/Serie5/Serie7/PersonWrite.txt");
		System.out.println("1/C/ Ecriture puis lecture dans un fichier texte :");
		pw.write(listPerson, "src/org/mohammad/serie07/exo15/PersonWriter.txt");
		pr.read("src/org/mohammad/serie07/exo15/PersonWriter.txt");
		System.out.println("\n");

		// Q2B
		System.out.println("2/B/ Ecriture dans un fichier binaire (Cr�ation du fichier) : ");
		pw.writeBinaryFields(listPerson, "BinaryField");
		System.out.println("");
		
		// Q2C
		List<Person> listBinaryField = new ArrayList<>();
		System.out.println("2/C/ Affichage du contenu du fichier binaire : ");
		listBinaryField = pr.readBinaryFields("BinaryField");
		listBinaryField.forEach(p -> System.out.println(p));
		System.out.println("");
		
		// Q3B
		System.out.println("3/B/ Ecriture dans un fichier binaire (Cr�ation du  fichier) : ");
		pw.writeBinaryObject(listPerson, "BinaryObject");
		System.out.println("");
		
		// Q3C
		List<Person> listBinaryObject = new ArrayList<>();
		System.out.println("3/C/ Affichage du contenu du fichier binaire : ");
		listBinaryObject = pr.readBinaryFields("BinaryField");
		listBinaryObject.forEach(p -> System.out.println(p));
		System.out.println("");
	}
}