package org.mohammad.serie07.exo15;

import java.util.*;
import java.io.*;

public class PersonReader 
{
	public List<Person> read(String filename)
	{
		List<Person> listPerson = new ArrayList<>();
		try
		{
			InputStream ips = new FileInputStream(filename);
			InputStreamReader ipsr = new InputStreamReader(ips);
			BufferedReader br = new BufferedReader(ipsr);
			String ligne;
			while ((ligne = br.readLine()) != null)
			{
				if(!ligne.startsWith("#"))
				{
					String[] tab = ligne.split(",");
					listPerson.add(new Person(tab[0],tab[1],Integer.parseInt(tab[2])));
				}
			}
			listPerson.forEach(p -> System.out.println(p));
			br.close(); 
		}
		catch(IOException e)
		{
			System.out.println("Erreur dans la lecture : " + e.getMessage());
		}
		
		return listPerson;
	}
	
	public List<Person> readBinaryFields(String fileName)
	{
		File fichier =  new File("src/org/mohammad/serie07/exo15/" + fileName);
		List<Person> listPerson = new ArrayList<>();
		long taille = fichier.length(); 
		
		try (FileInputStream fis =  new FileInputStream(fichier);
			DataInputStream dis = new DataInputStream(fis);)
		{
			for(long i = 0; i < taille; i++)
				listPerson.add(new Person(dis.readUTF(), dis.readUTF(), dis.readInt()));
		} 
		catch(EOFException e) 
		{
			return listPerson;
		} 
		catch(IOException e) 
		{
			System.out.println("e.getMessage() ="+ e.getMessage());
			e.printStackTrace();
		}
		
		return listPerson;
	}
	
	@SuppressWarnings("unchecked")
	public List<Person> readBinaryObject(String fileName)
	{
		File fichier =  new File("src/org/mohammad/serie07/exo15/" + fileName);
		List<Person> listPerson = new ArrayList<>();
		
		try (FileInputStream fis =  new FileInputStream(fichier);
				ObjectInputStream ois = new ObjectInputStream(fis);)
		{
			listPerson = (List<Person>) ois.readObject();
		} 
		catch (IOException e) 
		{
			System.out.println("Erreur : " + e.getMessage());
			e.printStackTrace();
		} 
		catch (ClassNotFoundException e) 
		{
			System.out.println("Erreur : " + e.getMessage());
			e.printStackTrace();
		}
		
		return listPerson;
	}
}