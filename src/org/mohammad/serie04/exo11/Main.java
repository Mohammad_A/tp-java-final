package org.mohammad.serie04.exo11;

import java.util.*;
import java.util.Comparator;

public class Main 
{
	public static void main(String[] args)
	{
		// Q1
		Comparator<String> compareLongueur = Comparator.comparing(s -> s.length());
		System.out.println("1/ Comparaison en fonction de la longueur des cha�nes :");
		System.out.println("abcde/abc : " + compareLongueur.compare("abcde", "abc") + "\n");
		
		Person p1 = new Person("Mohammad","Akash",21);
		Person p2 = new Person("Khan","Shahrukh",51);
		Person p3 = new Person("Mohammad","Tanuja",21);
		Person p4 = new Person("Khan","Tanuja",21);
		Person p5 = null;
		
		// Q2
		Comparator<Person> compareByLastName = Comparator.comparing(p -> p.getLastName());
		System.out.println("2/ Comparaison en fonction de lastName :");
		System.out.println("Mohammad/Khan : " + compareByLastName.compare(p1, p2) + "\n");
		
		// Q3
		Comparator<Person> compareThenByFirstName = compareByLastName.thenComparing(p -> p.getFirstName());
		System.out.println("3/ Comparaison en fonction du nom puis du pr�nom si nom identique : ");
		System.out.println("Mohammad Akash/Mohammad Tanuja : " + compareThenByFirstName.compare(p1, p3) + "\n");
		
		// Q4
		Comparator<Person> reverseCompare = compareThenByFirstName.reversed();
		System.out.println("4/ Comparateur inverse du comparateur pr�c�dent : ");
		System.out.println("Khan Tanuja/Mohammad Tanuja : " + reverseCompare.compare(p3, p4) + "\n");
		
		// Q5
		Comparator<Person> nullValues = Comparator.nullsLast(compareThenByFirstName);
		ArrayList<Person> listePersonnes = new ArrayList<>();
		
		System.out.println("5/ Trier les valeurs nulles, s'il y en a, � la fin de la liste :");
		listePersonnes.add(p5);
		listePersonnes.add(p4);
		listePersonnes.add(p3);
		listePersonnes.add(p2);
		listePersonnes.add(p1);
		System.out.println("Liste non tri�e : " + listePersonnes);
		listePersonnes.sort(nullValues);
		System.out.println("Liste tri�e : " + listePersonnes);
	} 
}