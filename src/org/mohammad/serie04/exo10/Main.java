package org.mohammad.serie04.exo10;

import java.util.function.Function;
import java.util.function.BiFunction;

public class Main 
{
	public static void main(String[] args)
	{
		// Q1
		Function<String,String> majuscule = s -> s.toUpperCase();
		System.out.println("1/ Mettre une chaine en majuscule : ");
		System.out.println("abcde : " + majuscule.apply("abcde"));
		
		// Q2
		Function<String,String> returnSameString = s -> s != (null) ? s:"";
		System.out.println("\n2/ Retourne la chaine sans modification : ");
		System.out.println("abcde : " + returnSameString.apply("abcde"));
		System.out.println("Chaine nulle : " + returnSameString.apply(null));
		
		// Q3
		Function<String,Integer> stringLength = s -> s != (null) ? s.length():0;
		System.out.println("\n3/ Taille d'une chaine :");
		System.out.println("abcde : " + stringLength.apply("abcde"));
		System.out.println("abcdef : " + stringLength.apply("abcdef"));
		
		// Q4
		Function<String,String> stringParenthese = s -> s != (null) ? "("+s+")":"()";
		System.out.println("\n4/ Chaine renvoy� avec des parenth�ses : ");
		System.out.println("abcde : " + stringParenthese.apply("abcde"));
		System.out.println("Chaine nulle : " + stringParenthese.apply(null));
		
		// Q5
		BiFunction<String,String,Integer> firstLetterPosition = (s1,s2) -> s1.indexOf(s2);
		System.out.println("\n5/ Position du premier caract�re de s1 si s2 est dans s1 : ");
		System.out.println("Bonjour / nj : " + firstLetterPosition.apply("Bonjour", "nj"));
		System.out.println("Bonjour / Hello : " + firstLetterPosition.apply("Bonjour","Hello"));
		
		// Q6 
		Function<String,Integer> positionGivenString = s -> firstLetterPosition.apply("abcdefghi",s); 
		System.out.println("\n6/ Position dans la chaine abcdefghi : "); 
		System.out.println("def : " + positionGivenString.apply("def"));
		System.out.println("ghi : " + positionGivenString.apply("ghi"));
	}
}