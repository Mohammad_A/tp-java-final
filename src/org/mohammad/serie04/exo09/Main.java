package org.mohammad.serie04.exo09;

import java.util.function.Predicate;

public class Main 
{
	public static void main(String[] args)
	{
		// Q1
		Predicate<String> longerThanFour = s -> s.length() > 4;
		System.out.println("1/ Taille de la cha�ne sup�rieur � 4 :");
		System.out.println("abc : " + longerThanFour.test("abc"));
		System.out.println("acbd : " + longerThanFour.test("abcd"));
		System.out.println("abcde : " + longerThanFour.test("abcde"));;
		System.out.println("");
		
		// Q2
		Predicate<String> isNotEmpty = s -> s.length() > 0;
		System.out.println("2/ Chaine de caract�re non vide");
		System.out.println("Chaine vide : " + isNotEmpty.test(""));
		System.out.println("abcde : " + isNotEmpty.test("abcde"));
		System.out.println("");
		
		// Q3
		Predicate<String> startWithJ = s -> s.charAt(0) == 'J';
		System.out.println("3/ Chaine commen�ant par la lettre J : ");
		System.out.println("ABC : " + startWithJ.test("ABC"));
		System.out.println(("JKL : " + startWithJ.test("JKL")));
		System.out.println("");
		
		// Q4
		Predicate<String> lengthIsFive = s -> s.length() == 5;
		System.out.println("4/ Taille de la chaine �gale � 5 :");
		System.out.println("abc : " + lengthIsFive.test("abc"));
		System.out.println("abcde : " + lengthIsFive.test("abcde"));
		System.out.println("");
		
		// Q5
		Predicate<String> is5J = lengthIsFive.and(startWithJ);
		System.out.println("5/ Chaine commence par J et de taille 5 :");
		System.out.println("ABCDE : " + is5J.test("abcde"));
		System.out.println("JKL : " + is5J.test("JKL"));
		System.out.println("JKLMN : " + is5J.test("JKLMN"));
	}
}